const express = require('express')
const routes = express.Router()

const UsersController = require("./controllers/UsersControllers")


routes.get('/users', UsersController.index)
    .post('/users', UsersController.store)
    .post('/login', UsersController.login)

module.exports = routes;