const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken');
const knex = require('../database/dbConfig')


module.exports = {

        async index(req, res) {

            const users = await knex('users');
            res.status(200).json(users)
        },

        async store(req, res) {

            const { name, email, password } = req.body;

            if (!name || !email || !password ) {
                res.status(400).json({ erro: "Preencha todos os campos" });
                return;
            }

            try {
                const userdata = await knex("users").where({ email });
                if (userdata.length) {
                    res.status(400).json({ erro: "E-mail já esta cadastrado" });
                    return
                }
            } catch (error) {
                res.status(400).json({ erro: error.message });
            }

            const hash = bcrypt.hashSync(password, 10);

            try {
                const newuser = await knex("users").insert({ name, email, password: hash });
                res.status(201).json({ id: newuser[0] });
            } catch (error) {
                res.status(400).json({ erro: error.message });
            }
        },
        async login(req, res) {
            const users = await knex('users');
            const { email, password } = req.body;

            if (!email || !password) {
                res.status(400).json({ erro: "Preencha todos os campos" });
                return;
            }

            try {
                const userdata = await knex("users").where({ email });
                if (!userdata.length) {
                    res.status(400).json({ erro: "Dados incorretos" });
                    return
                }

                if (bcrypt.compareSync(password, userdata[0].password)) {
                    const token = jwt.sign({
                        users_id: userdata[0].id,
                        users_name: userdata[0].name
                    }, process.env.JWT_KEY, {
                        expiresIn: "1h"
                    })
                    res.status(200).json({ msg: "Ok acesso permitido", token, users_name: userdata[0].name, users_id: userdata[0].id});
                } else {
                    res.status(400).json({ erro: "Dados incorretos" });
                }

            } catch (error) {
                res.status(400).json({ erro: error.message });
            }
        }

    }
    //parei no video duracao 1:34