exports.up = (knex) => {
    return knex.schema.createTable('users', (table) => {
        table.increments();
        table.string('name', 70).notNullable();
        table.string('email', 120).unique().notNullable();
        table.string('password', 60).notNullable();       
        table.timestamps(true, true)
    })
};

exports.down = (knex) => knex.schema.dropTable('users')
